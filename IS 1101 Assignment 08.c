#include <stdio.h>
struct student {
    int studentno;
    char fName[50];
    char subject[50];
    float marks;
} ;
int main() {
    int i;
    struct student s[10];
    printf("Enter information of students:\n");

        for (i = 0; i < 5; i++) {
        printf("Enter first name: ");
        scanf("%s", &s[i].fName);
        printf("Enter subject name: ");
        scanf("%s", &s[i].subject);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("\n");
    printf("Displaying Student Details:\n");

        for (i = 0; i < 5; i++) {
        printf("\nStudent number: %d\n", i + 1);
        printf("Name: %s \n",s[i].fName);
		printf("Subject: %s \n",s[i].subject);
		printf("Marks: %.2f \n",s[i].marks);
		printf("\n");
    }
    return 0;
}
